public class Pelanggan extends Manusia implements Makhluk{
    private int noPelanggan;
    
    public Pelanggan(String nama, int uang){
        setNama(nama);
        setUang(uang);
    }
    public Pelanggan(String nama, int uang, int umur){
        setNama(nama);
        setUang(uang);
        setUmur(umur);
    }
    public String beli(){
      return getNama() + " membeli makanan dan minuman di kedai VoidMain.";
    }
    @Override
    public String bicara(){
      return "Halo, saya " + getNama() + ". Uang saya adalah " + getUang() + ".";
    }
    public String bernafas(){
      return getNama() + " bernafas dengan paru-paru";
    }
    public String bergerak(){
      return getNama() + " bergerak dengan cara berjalan.";
    }
    public String toString(){
      return bicara();
    }
}
  