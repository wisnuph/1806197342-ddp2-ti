public class BurungHantu extends Hewan implements Makhluk{
    
    public BurungHantu(String nama, String spesies){
        setNama(nama);
        setSpesies(spesies);
    }
    public String bernafas(){
        return getNama() + " bernafas dengan paru-paru.";
    }
    public String bergerak(){
        return getNama() + " bergerak dengan cara terbang.";
    }
    public String bersuara(){
        return "Hooooh hoooooooh.(Halo, saya " + getNama() + ". Saya adalah burung hantu).";
    }
    public String toString(){
        return bersuara();
    }
}
