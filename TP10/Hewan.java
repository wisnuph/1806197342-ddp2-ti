public abstract class Hewan implements Makhluk  {
    private String nama;
    private String spesies;

    public Hewan(){
    }
    public Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setSpesies(String spesies){
        this.spesies = spesies;
    }
    public String getNama(){
        return nama;
    }
    public String getSpesies(){
        return spesies;
    }
    public abstract String bersuara();
}
