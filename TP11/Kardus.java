import java.util.ArrayList;
import java.util.*;
public class Kardus<T extends Barang> {
    private ArrayList<T> listBarang = new ArrayList<>();

    public Kardus(ArrayList<T> listBarang){
        this.listBarang = listBarang;
    }

    public String rekap(){
        int pakaian = 0;
        int elektronik =0;
        for(int i = 0; i<listBarang.size(); i++){
            try{
                ((Pakaian)listBarang.get(i)).toString();
                pakaian++;
            }catch(ClassCastException e){
                elektronik++;
            }
        }
        if(pakaian>0 && elektronik>0){
            return "Kardus Campuran: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }else if(pakaian>0){
            return "Kardus Pakaian: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }else{
            return "Kardus Elektronik: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }
    }
    public double getTotalValue(){
        double sum = 0.0;
        for(T barang : listBarang){
            try{
                sum += ((Pakaian)barang).getValue();
            }catch(ClassCastException e){
                sum += ((Elektronik)barang).getValue();
            }
        }
        return sum;
    }
    public void tambahBarang(T barang){
        listBarang.add(barang);
    }
    public ArrayList<T> getListBarang(){
        return this.listBarang;
    }


}
