import java.util.ArrayList;
public class MataKuliah {
    private String name;
    private String code;
    private ArrayList<Mahasiswa> daftarMhs = new ArrayList<>();
    private ArrayList<Manusia> daftarDosen = new ArrayList<>();

    public MataKuliah(String name, String code){
        this.name = name;
        this.code = code;
    }
    public void tambahMhs(Mahasiswa mhs){
        this.daftarMhs.add(mhs);
    }
    public void dropMhs(Mahasiswa mhs){
        if(this.daftarMhs.contains(mhs) == true){
            this.daftarMhs.remove(mhs);
        }
    }
    public void assignDosen(Manusia dosen){
        this.daftarDosen.add(dosen);
    }
    public String getNama(){
        return this.name;
    }
    public String getCode(){
        return this.code;
    }
    public Mahasiswa[] getDaftarMhs(){
        Mahasiswa[] obj = this.daftarMhs.toArray(new Mahasiswa[this.daftarMhs.size()]);
        return obj;
    }
    public String klasifikasiCode(){
        if(code.startsWith("UIGE") == true){
            return "Mata Kuliah Wajib Universitas";
        }else if(code.startsWith("UIST") == true){
            return "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        }else if(code.startsWith("CSGE") == true){
            return "Mata Kuliah Wajib Fakultas";
        }else if(code.startsWith("CSCM") == true){
            return "Mata Kuliah Wajib Program Studi Ilmu Komputer";
        }else if(code.startsWith("CSIM") == true){
            return "Mata Kuliah Wajib Program Studi Sistem Informasi";
        }else if(code.startsWith("CSCE") == true){
            return "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        }else if(code.startsWith("CSIE") == true){
            return "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        }else{
            return "Tidak ada klasifikasi Mata Kuliah tersebut";
        }
    }
    @Override
    public String toString(){
        return getNama() + " " + getCode();
    }
    
}
