import java.util.*;
public class Mahasiswa extends Manusia {
    private String npm;
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();

    public Mahasiswa(){}

    public Mahasiswa(String name, String npm){
       setNama(name);
       this.npm = npm;
    }
    public void tambahMatkul(MataKuliah matkul){
        this.daftarMatkul.add(matkul);
    }
    public void dropMatkul(MataKuliah matkul){
        if(this.daftarMatkul.contains(matkul) == true){
            this.daftarMatkul.remove(matkul);
        }
    }
    public String getNpm(){
        return this.npm;
    }
    public MataKuliah[] getDaftarMatkul(){
        MataKuliah[] obj = this.daftarMatkul.toArray(new MataKuliah[this.daftarMatkul.size()]);
        return obj;
    }
    public void cetakMatkul(MataKuliah o){
        o.klasifikasiCode();
    }
    @Override
    public String toString(){
        return getNama() + " " + getNpm();
    }
    
}
