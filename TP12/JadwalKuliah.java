public class JadwalKuliah extends GenericMatrix<String>{
    @Override
    protected String add(String s1, String s2){
        if(s1.equals(s2) == true){
            return s1;
        }else if(s1.equals("-") && !s2.equals("-")){
            return s2;
        }else if(!s1.equals("-") && s2.equals("-")){
            return s1;
        }else{
            return "-";
        }
    }
}
