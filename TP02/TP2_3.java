import java.util.Scanner;

public class TP2_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Selamat datang di desa Dedepedua");
        System.out.print("Apa hewan kesukaan Kepala Desa? ");
        try {
            String hewan = input.nextLine();
            if (hewan.equals("burung hantu")) {
                System.out.print("Berapa jumlah harimau di desa Dedepedua? ");
                int total = input.nextInt();
                if (total == 8 || total == 10 || total == 18) {
                    System.out.print("Berapa kecepatan harimau di desa Dedepedua? ");
                    Double velocity = input.nextDouble();
                    if ( 100.0 < velocity && velocity < 120.0) {
                        System.out.print("Selamat kamu boleh masuk");
                    } else {
                        System.out.println("Kamu dilarang masuk");
                    }
                } else {
                    System.out.println("Kamu dilarang masuk");
                }
            } else {
                System.out.println("Kamu dilarang masuk");
            }
        } catch (Exception InputMismatchException) {
            System.out.println("Masukkan input sesuai perintah!");
        }
    }
}
